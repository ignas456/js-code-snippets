function groupBy(array, groupPredicate) {
    let result = {};

    array.forEach(function (element, index) {

        var predicateResult = groupPredicate(element);
        if (!result[predicateResult])
            result[predicateResult] = [];
        result[predicateResult].push(element);
    });

    return result;
}